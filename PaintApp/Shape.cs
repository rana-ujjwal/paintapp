﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp
{
    /// <summary>
    /// Abstract class Shape which declares the factory method
    /// </summary>
    public abstract class Shape : ShapeInterface
    {
        /// <summary>
        /// starting color
        /// </summary>
        protected Color color;
        /// <summary>
        /// starting fill
        /// </summary>
        protected bool fill;
        /// <summary>
        /// starting co-ordinate
        /// </summary>
        protected int x, y;
        /// <summary>
        /// starting flash
        /// </summary>
        protected string flash;


        /// <summary>
        /// Default constructor
        /// </summary>
        public Shape()
        {
            color = Color.Red;
            x = y = 100;
        }

        /// <summary>
        /// Overloading constructor with parameters
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape(Color color, bool fill, int x, int y)
        {
            this.color = color;
            this.fill = fill;
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Overloading constructor with parameters
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape(string flash, bool fill, int x, int y)
        {
            this.flash = flash;
            this.fill = fill;
            this.x = x;
            this.y = y;
        }

        public abstract void draw(Graphics g);

        /// <summary>
        /// Override set method derived from base class
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public virtual void set(Color color, bool fill, params int[] list)
        {
            this.color = color;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }

        /// <summary>
        /// Override set method derived from base class
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public virtual void set(string flash, bool fill, params int[] list)
        {
            this.flash = flash;
            this.fill = fill;
            this.x = list[0];
            this.y = list[1];
        }

        /// <summary>
        /// Implementation of ToString method that returns a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        { 
            return base.ToString() + " " + this.x + "," + this.y + " : ";
        }
    }
}
