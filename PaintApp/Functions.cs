﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace PaintApp
{
    class Functions
    {
        private static Dictionary<string, int> _functions = new Dictionary<string, int>();
        public static Dictionary<string, int> Funcs
        {
            get
            {
                return _functions;
            }
            set
            {
                _functions = value;
            }
        }
        public static void Clear()
        {
            _functions = new Dictionary<string, int>();
        }

        /// <summary>
        /// Searches functions
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public static int SearchFunction(string var)
        {
            int result;
            if (!_functions.TryGetValue(var, out result))
            {
                MessageBox.Show("Invalid function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }
    }
}
