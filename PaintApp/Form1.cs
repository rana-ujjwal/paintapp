﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace PaintApp
{
    /// <summary>
    /// Class Form1 inherits base cass Form
    /// </summary>
    public partial class Form1 : Form
    {
        Parser parser;
        private string filePath = null;
        private bool fileSaved = true;
        private bool runProgram = true;
        private int[] loopList = new int[10];
        private int loopIndex = -1;
        private bool methodRunning = false;
        private int tempProgramIndex = -1;

        Graphics g;
        /// <summary>
        /// Form1 function
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            g = outputCanvas.CreateGraphics();
            parser = new Parser(g);
        }

        LogicParser logicParser = new LogicParser();

        /// <summary>
        /// Function that resets
        /// </summary>
        private void resetAll()
        {
            loopIndex = -1;
            runProgram = true;
            Array.Clear(loopList, 0, 10);
            Variable.Clear();
        }

        /// <summary>
        /// Creating function for Run button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRun_Click(object sender, EventArgs e)
        {
            resetAll();
            for (int i = 0; i < textBox.Lines.Length; i++)
            {
                try
                {
                    char[] seperator = new char[] { ' ' };
                    string[] args = textBox.Lines[i].Trim().Split(seperator, 2);
                    string command = args[0].ToLower();
                    string parameters = "";
                    if (args.Length > 1)
                    {
                        parameters = args[1];
                    }
                    if (methodRunning)
                    {
                        if (command == "endmethod") 
                        {
                            methodRunning = false; 
                        }
                    }
                    else
                    {
                        if (command == "while")
                        {
                            loopList[++loopIndex] = i;
                            continue;
                        }
                        else if (command == "endwhile")
                        {
                            if (loopIndex > -1)
                            {
                                string[] commandParse = textBox.Lines[loopList[loopIndex]].Trim().Split(seperator, 2);
                                string parsedCommand = commandParse[0].ToLower();
                                string parsedParameters = "";

                                if (commandParse.Length > 1)
                                {
                                    parsedParameters = commandParse[1];
                                }
                                string condition = (parsedParameters.Trim());
                                if (logicParser.Parse(condition))
                                {
                                    i = loopList[loopIndex];
                                }
                                else
                                    loopIndex--;
                            }
                            continue;
                        }
                        else if (command == "if")
                        {
                            Regex pattern = new Regex(@"(.+?)then (.+)");
                            Match match = pattern.Match(parameters);
                            if (match.Success)
                            {
                                if (logicParser.Parse(match.Groups[1].Value))
                                {
                                    parser.parse(match.Groups[2].Value.Trim());
                                }
                                continue;
                            }

                            string condition = (parameters.Trim());
                            if (!logicParser.Parse(condition))
                            {
                                runProgram = false;
                            }
                            continue;
                        }
                        else if (command == "endif")
                        {
                            runProgram = true;
                            continue;
                        }
                        else if (command == "method")
                        {
                            if (parameters != null)
                            {
                                methodRunning = true;
                                if (Functions.Funcs.ContainsKey(command))
                                {
                                    MessageBox.Show("Function Already Exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                                else
                                {
                                    try
                                    {
                                        Functions.Funcs.Add(parameters.Split(',')[0].Trim(), i);
                                    }
                                    catch (Exception)
                                    {
                                        MessageBox.Show("Error Defining Function", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        break;
                                    }
                                }
                                continue;
                            }
                        }
                        else if (command == "endmethod")
                        {
                            i = tempProgramIndex;
                            tempProgramIndex = 0;
                            continue;
                        }
                        else if (command == "call")
                        {
                            tempProgramIndex = i;
                            i = Functions.SearchFunction(parameters.Split(',')[0].Trim());
                            continue;
                        }
                        else
                        {
                            if (runProgram)
                            {
                                parser.parse(textBox.Lines[i].Trim());
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
            }
        }
        /// <summary>
        ///  Creating function for Save button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        /// <summary>
        ///  Creating saveFile function to save file
        /// </summary>
        /// <returns></returns>
        private bool saveFile()
        {
            string code = textBox.Text;
            if (filePath == null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "gpl files (*.gpl)|*.gpl|All files (*.*)|*.*";
                saveFileDialog.Title = "Save GPL";
                saveFileDialog.ShowDialog();

                if (saveFileDialog.FileName != "")
                {
                    filePath = saveFileDialog.FileName;
                    using (StreamWriter sw = new StreamWriter(filePath))
                        sw.WriteLine(code);
                    fileSaved = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                using (StreamWriter sw = new StreamWriter(filePath))
                    sw.WriteLine(code);
                fileSaved = true;
                return true;
            }

        }

        /// <summary>
        /// Function for Form1_FormClosing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!fileSaved)
            {
                DialogResult dialogResult = MessageBox.Show("Save current file before proceeding?", "Changes not saved", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!saveFile())
                        e.Cancel = true;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Function for NewToolStripMenuItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            g.Clear(Color.Gray);
            command.Text = "";
            textBox.Text = "";
            parser.clear();
        }

        /// <summary>
        /// Function for loadToolStripMenuItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!fileSaved)
            {
                DialogResult dialogResult = MessageBox.Show("Save current file before proceeding?", "Changes not saved", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!saveFile())
                        return;
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }
            }
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\Desktop";
                openFileDialog.Filter = "gpl files (*.gpl)|*.gpl|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;

                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        textBox.Text = reader.ReadToEnd();
                    }

                    fileSaved = true;
                }
            }
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        /// <summary>
        /// Function for ExitToolStripMenuItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Function for Command_KeyDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void command_KeyDown(object sender, KeyEventArgs e)
        {
            string cmd = command.Text.ToLower().Split(' ')[0];
            if (e.KeyCode == Keys.Enter)
            {
                switch (cmd)
                {
                    case "clear":
                        g.Clear(Color.Gray);
                        parser.clear();
                        break;
                    case "reset":
                        g.Clear(Color.Gray);
                        textBox.Text = "";
                        command.Text = "";
                        parser.clear();
                        break;
                    case "run":
                        btnRun_Click(sender, e);
                        break;
                    default:
                        MessageBox.Show("Invalid command", "Error!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
                e.SuppressKeyPress = true;
            }
        }

        /// <summary>
        /// Function for AboutToolStripMenuItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Developed By:-\nUjjwal Ranamagar\n Version: 1.0.0", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
