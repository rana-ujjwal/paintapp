﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp
{
    class LogicParser
    {
        ArithmeticParser arithmeticParser = new ArithmeticParser();

        /// <summary>
        /// Function for logical expressions
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public bool Parse(string expression)
        {
            string[] operands = expression.Split(new string[] { "==", "!=", "<=", ">=", "<", ">" },
                StringSplitOptions.RemoveEmptyEntries);
            if (operands.Length == 0)
            {
                MessageBox.Show("Invalid Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (operands.Length == 1)
            {
                return false;
            }
            else
            {
                string Operator = expression.Replace(operands[0], "").Replace(operands[1], "").Trim();
                if (Operator.Length == 0)
                {
                    MessageBox.Show("Invalid Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                int value1 = arithmeticParser.Parse(operands[0].Trim());
                int value2 = arithmeticParser.Parse(operands[1].Trim());

                switch (Operator)
                {
                    case "==":
                        return value1 == value2;
                    case "!=":
                        return value1 != value2;
                    case "<":
                        return value1 < value2;
                    case ">":
                        return value1 > value2;
                    case "<=":
                        return value1 <= value2;
                    case ">=":
                        return value1 >= value2;
                }
            }
            return false;
        }
    }
}
