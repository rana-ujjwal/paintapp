﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp
{
    /// <summary>
    /// Class DrawTo inherits base class Shape.
    /// </summary>
    public class DrawTo : Shape
    {
        int x2, y2;
        public DrawTo() : base()
        {

        }
        /// <summary>
        /// Overloading Constructor and call overloaded base constructor.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        public DrawTo(Color color, bool fill, int x, int y, int x2, int y2) : base(color, fill, x, y)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        /// <summary>
        /// New implementation of Draw method that is inherited from a base class.
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(color, 3);
            g.DrawLine(p, x, y, x2, y2);
        }

        /// <summary>
        /// New implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.x2 = list[2];
            this.y2 = list[3];
        }
    }
}
