﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace PaintApp
{
    /// <summary>
    /// ShapeInterface
    /// </summary>
    interface ShapeInterface
    {
        void set(Color c, bool fill, params int[] list);
        void set(string flash, bool fill, params int[] list);
        void draw(Graphics g);
    }
}
