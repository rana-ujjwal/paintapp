﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace PaintApp
{  
    /// <summary>
    /// Class Rectangle inherits base class Shape.
    /// </summary>
   public class Rectangle : Shape
    {
        int width, height;

        /// <summary>
        /// Call base class constructor.
        /// </summary>
        public Rectangle() : base()
        {

        }

        /// <summary>
        /// Overloading constructor and call overloaded basse constructor
        /// </summary>
        /// <param name="color">color of rectangle</param>
        /// <param name="fill">fill enabled (true/false)</param>
        /// <param name="x">x-axis of rectangle</param>
        /// <param name="y">y-axis of rectangle</param>
        /// <param name="width">width of rectangle</param>
        /// <param name="height">height of rectangle</param>
        public Rectangle(Color color, bool fill, int x, int y, int width, int height) : base(color, fill, x, y)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Overloading constructor and call overloaded basse constructor
        /// </summary>
        /// <param name="flash">flash color of rectangle</param>
        /// <param name="fill">fill enabled (true/false)</param>
        /// <param name="x">x-axis of rectangle</param>
        /// <param name="y">y-axis of rectangle</param>
        /// <param name="width">width of rectangle</param>
        /// <param name="height">height of rectangle</param>
       public Rectangle(string flash, bool fill, int x, int y, int width, int height) : base(flash, fill, x, y)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// New implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }

        /// <summary>
        /// New implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }

        /// <summary>
        /// New implementation of Draw method that is inherited from a base class.
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new Thread(() => StartFlashing(g, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new Thread(() => StartFlashing(g, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new Thread(() => StartFlashing(g, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invald color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else
                {
                    SolidBrush b = new SolidBrush(color);
                    g.FillRectangle(b, x, y, width, height);
                }
            }
            else
            {
                Pen p = new Pen(Color.Black, 2);
                g.DrawRectangle(p, x, y, width, height);
            }
        }

        /// <summary>
        /// Function that enables flashing
        /// </summary>
        /// <param name="g"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        private void StartFlashing(Graphics g, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new SolidBrush(first);
                        g.FillRectangle(b, x, y, width, height);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new SolidBrush(second);
                        g.FillRectangle(b, x, y, width, height);
                        flag = false;
                    }
                }
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Implementation of ToString method that returns a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.width + " " + this.height;
        }
    }
}
