﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace PaintApp
{
    class ArithmeticParser
    {
        /// <summary>
        /// Function for arithmetic exxpressions
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public int Parse(string expression)
        {
            string[] operands = expression.Split(new string[] { "+", "-", "*", "/", "%" },
            StringSplitOptions.RemoveEmptyEntries);

            if (operands.Length == 0)
            {
                MessageBox.Show("Not sufficient parameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (operands.Length == 1)
            {
                return Variable.FindVariable(operands[0]);
            }
            else
            {
                string Operator = expression.Replace(operands[0], "").Replace(operands[1], "").Trim();
                if (Operator.Length == 0)
                {
                    MessageBox.Show("Invalid expression", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                int value1 = Variable.FindVariable(operands[0].Trim());
                int value2 = Variable.FindVariable(operands[1].Trim());

                switch (Operator)
                {
                    case "+":
                        return value1 + value2;
                    case "-":
                        return value1 - value2;
                    case "*":
                        return value1 * value2;
                    case "/":
                        return value1 / value2;
                    case "%":
                        return value1 % value2;
                }
            }
            return 0;
        }
    }
}
