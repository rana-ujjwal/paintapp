﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PaintApp
{
    
    /// <summary>
    /// Creating Parser class to parse the input.
    /// </summary>
    public class Parser
    {
        /// <summary>
        /// Graphics
        /// </summary>
        private Graphics g;

        /// <summary>
        /// Empty constructor
        /// </summary>
        public Parser()
        {

        }

        /// <summary>
        /// Parser Graphics
        /// </summary>
        /// <param name="g"></param>
        public Parser(Graphics g)
        {
            this.g = g;
        }
        /// <summary>
        /// starting co-ordinates
        /// </summary>
        private int x = 0, y = 0;
        /// <summary>
        /// starting color
        /// </summary>
        private Color color = Color.Black;
        /// <summary>
        /// starting fill
        /// </summary>
        private bool fill;
        /// <summary>
        /// starting flash
        /// </summary>
        private string flash = null;
        /// <summary>
        /// new ArithmeticParser object
        /// </summary>
        static ArithmeticParser arithmeticParser = new ArithmeticParser();

        /// <summary>
        /// x cordinate getter
        /// </summary>
        public int X
        {
            get { return x;  }
        }

        /// <summary>
        /// y cordinate getter
        /// </summary>
        public int Y
        {
            get { return y; }
        }

        /// <summary>
        /// color getter
        /// </summary>
        public Color Color
        {
            get { return color; }
        }

        /// <summary>
        /// fill getter
        /// </summary>
        public bool Fill
        {
            get { return fill; }
        }

        /// <summary>
        /// Creating clear function with its attributes.
        /// </summary>
        public void clear()
        {
            x = 0;
            y = 0;
            color = Color.Black;
            fill = false;
            flash = null;
        }

        /// <summary>
        /// Function to pass the input.
        /// </summary>
        /// <param name="cmd"></param>
        public void parse(string cmd)
        {
            ShapeFactory factory = new ShapeFactory();
            try
            {
                char[] seperator = { ' ' };
                string[] args = cmd.Split(seperator, 2);
                string command = args[0];
                string parameters = "";
                if (args.Length > 1)
                {
                    parameters = args[1];
                }
                switch (command.ToLower())
                {
                    case "":
                        break;
                    case "circle":
                        int radius = arithmeticParser.Parse(parameters.Trim());
                        Shape circle = factory.getShape("circle");
                        if(flash != null)
                        {
                            circle.set(flash, fill, x, y, radius);
                        }
                        else
                        {
                            circle.set(color, fill, x, y, radius);
                        }
                        circle.draw(g);
                        break;
                    case "rectangle":
                        int width = arithmeticParser.Parse(parameters.Split(',')[0].Trim());
                        int height = arithmeticParser.Parse(parameters.Split(',')[1].Trim());
                        Shape rectangle = factory.getShape("rectangle");
                        if(flash !=null)
                        {
                            rectangle.set(flash, fill, x, y, width, height);
                        }
                        else
                        {
                            rectangle.set(color, fill, x, y, width, height);
                        }
                        rectangle.draw(g);
                        break;
                    case "triangle":
                        int x2 = arithmeticParser.Parse(parameters.Split(',')[0].Trim());
                        int y2 = arithmeticParser.Parse(parameters.Split(',')[1].Trim());
                        int x3 = arithmeticParser.Parse(parameters.Split(',')[2].Trim());
                        int y3 = arithmeticParser.Parse(parameters.Split(',')[3].Trim());
                        Shape triangle = factory.getShape("triangle");
                        if(flash != null)
                        {
                            triangle.set(flash, fill, x, y, x2, y2, x3, y3);
                        }
                        else
                        {
                            triangle.set(color, fill, x, y, x2, y2, x3, y3);
                        }
                        triangle.draw(g);
                        break;
                    case "moveto":
                        x = arithmeticParser.Parse(parameters.Split(',')[0].Trim());
                        y = arithmeticParser.Parse(parameters.Split(',')[1].Trim());
                        break;
                    case "drawto":
                        int xaxis2 = arithmeticParser.Parse(parameters.Split(',')[0].Trim());
                        int yaxis2 = arithmeticParser.Parse(parameters.Split(',')[1].Trim());
                        Shape line = factory.getShape("drawto");
                        line.set(color, fill, x, y, xaxis2, yaxis2);
                        line.draw(g);
                        break;
                    case "color":
                        try
                        {
                            if (parameters.Split(',').Length == 1)
                            {
                                if (parameters == "redgreen" || parameters == "blueyellow" || parameters == "blackwhite")
                                {
                                    flash = parameters;
                                }
                                else
                                {
                                    color = Color.FromName(parameters.Trim());
                                    flash = null;
                                }
                            }
                            else if (parameters.Split(',').Length == 3)
                            {
                                int red = arithmeticParser.Parse(parameters.Split(',')[0].Trim());
                                int green = arithmeticParser.Parse(parameters.Split(',')[1].Trim());
                                int blue = arithmeticParser.Parse(parameters.Split(',')[2].Trim());
                                color = Color.FromArgb(255, red, green, blue);
                                flash = null;
                            }
                            else if (parameters.Split(',').Length == 4)
                            {
                                int alpha = arithmeticParser.Parse(parameters.Split(',')[0].Trim());
                                int red = arithmeticParser.Parse(parameters.Split(',')[1].Trim());
                                int green = arithmeticParser.Parse(parameters.Split(',')[2].Trim());
                                int blue = arithmeticParser.Parse(parameters.Split(',')[3].Trim());
                                color = Color.FromArgb(alpha, red, green, blue);
                                flash = null;
                            }
                            else
                            {
                                MessageBox.Show("Invalid Color Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        catch (FormatException)
                        {
                            MessageBox.Show("Invalid Color Value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    case "fill":
                        {
                            fill = bool.Parse(parameters.Split(',')[0].Trim());
                            break;
                        }
                    case "clear":
                        {
                            g.Clear(Color.Gray);
                            break;
                        }
                    case "var":
                        char[] separator = { ' ' };
                        string[] expression = parameters.Split(separator, 2);
                        if (Variable.Variables.ContainsKey(expression[0].Trim()))
                        {
                            MessageBox.Show("Invalid Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            try
                            {
                                Variable.Variables.Add(expression[0].Trim(), arithmeticParser.Parse(expression[1].Trim()));
                            }
                            catch (Exception)
                            {
                                Variable.Variables.Add(expression[0].Trim(), 0);
                            }
                        }
                        break;
                    default:
                        if (command.Contains('='))
                        {
                            string[] splits = command.Split('=');
                            Variable.Variables[splits[0].Trim()] = arithmeticParser.Parse(splits[1].Trim());
                        }
                        else if (parameters.Split(',')[0].Trim().Contains('='))
                        {
                            Variable.Variables[command] = arithmeticParser.Parse(parameters.Split(',')[0].Trim().Substring(1).Trim());
                        }
                        else
                            MessageBox.Show("Unknown Command", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            catch (FormatException)
            {
                MessageBox.Show($"Invalid argument type", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
