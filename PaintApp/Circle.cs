﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PaintApp
{
    /// <summary>
    /// Class Circle inhertis base class Shape.
    /// </summary>
    public class Circle : Shape
    {
        int radius;

        /// <summary>
        /// Call base class constructor.
        /// </summary>
        public Circle() : base()
        {

        }

        /// <summary>
        /// Overloading Constructor with parameters
        /// </summary>
        /// <param name="color">color of the circle</param>
        /// <param name="fill">fill enabled (true/false)</param>
        /// <param name="x">x-axis of the circle</param>
        /// <param name="y">y-axis of the circle</param>
        /// <param name="radius">radius of the circle</param>
        public Circle(Color color, bool fill, int x, int y, int radius) : base(color, fill, x, y)
        {
            this.radius = radius;
        }

        /// <summary>
        /// Overloading Constructor with parameters
        /// </summary>
        /// <param name="flash">flash color of the circle</param>
        /// <param name="fill">fill enabled (true/false)</param>
        /// <param name="x">x-axis of the circle</param>
        /// <param name="y">y-axis of the circle</param>
        /// <param name="radius">radius of the circle</param>
        public Circle(string flash, bool fill, int x, int y, int radius) : base(flash, fill, x, y)
        {
            this.radius = radius;
        }

        /// <summary>
        /// New implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(Color color, bool fill, params int[] list)
        {
            base.set(color, fill, list[0], list[1]);
            this.radius = list[2];
        }

        /// <summary>
        /// New implementation of Set method that is inherited from a base class.
        /// </summary>
        /// <param name="flash"></param>
        /// <param name="fill"></param>
        /// <param name="list"></param>
        public override void set(string flash, bool fill, params int[] list)
        {
            base.set(flash, fill, list[0], list[1]);
            this.radius = list[2];
        }
        /// <summary>
        /// New implemenation of Draw method that is inherited from a base class.
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {

            if (fill)
            {
                if (flash != null)
                {
                    Thread newThread;
                    switch (flash)
                    {
                        case "redgreen":
                            newThread = new Thread(() => StartFlashing(g, Color.Red, Color.Green));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blueyellow":
                            newThread = new Thread(() => StartFlashing(g, Color.Blue, Color.Yellow));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        case "blackwhite":
                            newThread = new Thread(() => StartFlashing(g, Color.Black, Color.White));
                            newThread.IsBackground = true;
                            newThread.Start();
                            break;
                        default:
                            MessageBox.Show("Invald color value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                    }
                }
                else {
                    SolidBrush b = new SolidBrush(color);
                    g.FillEllipse(b, x, y, radius * 2, radius * 2); 
                }
            }
            else
            {
                Pen p = new Pen(color, 2);
                g.DrawEllipse(p, x, y, radius * 2, radius * 2);
            }
        }

        /// <summary>
        /// Function that enables flashing
        /// </summary>
        /// <param name="g"></param>
        /// <param name="first"></param>
        /// <param name="second"></param>
        private void StartFlashing(Graphics g, Color first, Color second)
        {
            bool flag = false;
            while (true)
            {
                lock (g)
                {
                    if (flag == false)
                    {
                        SolidBrush b = new SolidBrush(first);
                        g.FillEllipse(b, x, y, radius * 2, radius * 2);
                        flag = true;
                    }
                    else
                    {
                        SolidBrush b = new SolidBrush(second);
                        g.FillEllipse(b, x, y, radius * 2, radius * 2);
                        flag = false;
                    }
                }
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Implementation of ToString method that returns a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + " " + this.radius;
        }
    }
}
