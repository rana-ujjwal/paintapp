﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace PaintApp
{
    /// <summary>
    /// Variables class to store all variables and parameters
    /// </summary>
   public class Variable
    {
        /// <summary>
        /// Stores all program variables in runtime
        /// </summary>
        public static Dictionary<string, int> _variables = new Dictionary<string, int>();

        /// <summary>
        /// Gets and sets values of variables
        /// </summary>
        public static Dictionary<string, int> Variables
        {
            get { return _variables; }
            set { _variables = value; }
        }

        /// <summary>
        /// Clears all the stored variables
        /// </summary>
        public static void Clear()
        {
            _variables = new Dictionary<string, int>();
        }
        /// <summary>
        /// Function to find variables
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        public static int FindVariable(string var) //search through defined variables
        {
            int result;
            if (!_variables.TryGetValue(var, out result))
            {
                try
                {
                    result = int.Parse(var);
                }
                catch (FormatException)
                {
                    MessageBox.Show("Invalid Constant", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return result;

        }
    }
}
