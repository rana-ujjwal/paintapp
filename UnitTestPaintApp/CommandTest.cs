﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaintApp;
using System.Drawing;

namespace UnitTestPaintApp
{
    [TestClass]
    public class CommandTest
    {
        readonly Parser parser = new Parser();

        [TestMethod]
        public void MoveToTest()
        {
            string cmd = "moveto 50, 100";
            parser.parse(cmd);
            Assert.AreEqual(50, parser.X);
            Assert.AreEqual(100, parser.Y);
        }

        [TestMethod]
        public void FillTest()
        {
            string cmd = "fill true";
            parser.parse(cmd);
            Assert.AreEqual(true, parser.Fill);
        }

        [TestMethod]
        public void ColorTest()
        {
            string cmd = "color red";
            parser.parse(cmd);
            Assert.AreEqual(Color.Red, parser.Color);
        }

        [TestMethod]
        public void ClearTest()
        {
            parser.clear();
            Assert.AreEqual(0, parser.X);
            Assert.AreEqual(0, parser.Y);
            Assert.AreEqual(Color.Black, parser.Color);
            Assert.AreEqual(false, parser.Fill);
        }

        [TestMethod]
        public void VarTest()
        {
            string cmd = "var i";
            parser.parse(cmd);
            Assert.IsTrue(Variable.Variables.ContainsKey("i"));
        }
    }
}
